$(document).ready(function(){
    $(':input[type="submit"]').prop('disabled', true);
    $("#select1").change(function(){

        if($(this).val() == 1){
            $("#typeslct").hide();
            $("#typeslct").hide();
            $("#price_month").val("");
        }else if ($(this).val() == 2) {
            $("#typeslct").show();
            $("#price_month").val("");
            $("#select2").change(function(){
                let inp = $(this).val();
                switch (inp) {
                    case "1":
                        $("#price_month").val("");
                        $(':input[type="submit"]').prop('disabled', true);
                        break;
                    case "2":
                        $("#price_month").val(30);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                    case "3":
                        $("#price_month").val(60);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                    case "4":
                        $("#price_month").val(90);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                    case "5":
                        $("#price_month").val(120);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                }

            });

        } else {
            $("#typeslct").show();
            $("#price_month").val("");

            $("#select2").change(function(){
                let inp = $(this).val();
                //console.log(inp);
                switch (inp) {
                    case "1":
                        $("#price_month").val("");
                        $(':input[type="submit"]').prop('disabled', true);
                        break;
                    case "2":
                        $("#price_month").val(60);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                    case "3":
                        $("#price_month").val(120);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                    case "4":
                        $("#price_month").val(180);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                    case "5":
                        $("#price_month").val(240);
                        $(':input[type="submit"]').prop('disabled', false);
                        break;
                }

            });
        }

    });


});
