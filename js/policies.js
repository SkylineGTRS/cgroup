var insurancePoliciesHelper = {
	config : {
		Prices : {
			LugaggePrice      : 12,
			FlightDelayPrice  : 6,
			DayPrices 		  : {
				BeforeAgePointPrice : 1.0,
				AfterAgePointPrice  : 2.0
			},
			MinDayPrices : {
				BeforeAgePointPrice : 5,
				AfterAgePointPrice  : 10
			},
			DayPrice : 1.0,
			CurrentMinPrice : 5.0
		},
		Ages : {
			DayPriceChangeAgePoint : 65,
			MinAgePoint : 1, 
		},
		Days : {
			MinDaysNum   : 5,
			MinThreshold : 180
		},
		DateFormat : 'DD/MM/YYYY',
		Markup : {
			Admin : {
				Blocks : {
					CompanyUserActive   	 : '<p class="green">აქტიური</p>',
					CompanyUserInactive 	 : '<p class="red">არააქტიური</p>',
					ActivationRequestNotSent : '<a class="link" id="company-activation-btn" onclick="insurancePoliciesHelper.requestActivation()">სერვისის აქტივაციის მოთხოვნა</a>',
					ActivationRequestSent    : '<a class="link active" id="company-activation-btn"><i class="fa fa-check"></i>თქვენი მოთხოვნა გაგზავნილია</a>'	
				},
				Actions : {
					ActivateCompanyUser   : `<a class="btn btn-default tbc-manage-company-status" onclick="insurancePoliciesHelper.setCompanyStatus(%s, 1)">
			                                    <i class="fa fa-power-off green" aria-hidden="true"></i>
			                                    <span>აქტივაცია</span>
			                                 </a>`,
					DeactivateCompanyUser : `<a class="btn btn-default tbc-manage-company-status" onclick="insurancePoliciesHelper.setCompanyStatus(%s, 0)">
			                                    <i class="fa fa-power-off red" aria-hidden="true"></i>
			                                    <span>დეაქტივაცია</span>
			                                 </a>`
				},
			}
		}
	},
	datepickerLangs : {
		mNames : [ "იანვარი", "თებერვალი", "მარტი", "აპრილი",
                   "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი",
                   "ოქტომბერი", "ნოემბერი", "დეკემბერი" ],
        dNames : ['ორშაბათი', 'სამშაბათი', 'ოთხშაბათი', 'ხუთშაბათი', 'პარასკევი', 'შაბათი', 'კვირა'],
        dNamesShort : ['ორ.', 'სამ.', 'ოთხ.', 'ხუთ.', 'პარ.', 'შაბ.', 'კვ.']
	},
	fields : {
		legalCompNameGeo    : '#LegalCompanyName',
		legalAddressNameGeo : '#LegalAddressName',
		legalInfoForm	    : '#legal-info-form',
		names : {
			insuredNameEng     : 'InsuredNameEng',
			insuredNameGeo     : 'InsuredNameGeo',
			insuredLastnameEng : 'InsuredLastnameEng',
			insuredLastnameGeo : 'InsuredLastnameGeo',
			passportNumber	   : 'PassportNumber',
			address 		   : 'Address',
			dateFromPicker     : 'DateFromPicker',
			dateToPicker 	   : 'DateToPicker',
			birthdayPicker 	   : 'BirthdayPicker',
			notResident		   : 'NotResident',
			idCode 			   : 'IDCode',
			limitCurrency 	   : 'LimitCurrency',
			userBonus 		   : 'UserBonusValue',
			mobile			   : 'Mobile',
			insuredDays		   : 'InsuredDaysNum',
			policyType 		   : 'PolicyType',
			luggage 		   : 'Luggage',
			flightDelay 	   : 'FlightDelay',
			filterStartDate    : 'StartDate',
			filterEndDate 	   : 'EndDate'
		}
	},
	tags : {
		newPolicyForm    		: '#new-policy-form',
		deductable 	     		: '#Deductable',
		minBonusValue    		: '#BonusValue',
		filterForm	     		: '#tbc-policies-filter-form',
		tbcPageBlock     		: '#tbc-page-block',
		operatorsList    		: '#operators-list',
		tbcCompListItem  		: 'data-tbc-user-id',
		tbcCompStatusBlock 		: '.tbc-company-status-block',
		tbcServiceStatusBlock 	: '.tbc-service-status-block',
		manageCompStatusBtn 	: '.tbc-manage-company-status',
		companyActivationBtn 	: '#company-activation-btn'
	},
	eventListeners : {
		onResidentChange : function(){
			var isChecked = $(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.notResident+'"]').is(':checked');
				insurancePoliciesHelper.idCodeRequired(!isChecked);
		},
		onLimitCurrencyChange : function() {
			var deductable = $(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.limitCurrency+'"]').is(':checked')
								? '100 ევრო/EURO' 
								: '100 აშშ დოლარი/USD';
			$(insurancePoliciesHelper.tags.newPolicyForm).find(insurancePoliciesHelper.tags.deductable).html(deductable);
			
		},
		onPolicyTypeChange : function() {
			var f = $(insurancePoliciesHelper.tags.newPolicyForm);
			var policyType = f.find('select[name="'+insurancePoliciesHelper.fields.names.policyType+'"]').val();
				(parseInt(policyType) == 2)
					? insurancePoliciesHelper.multiPolicyType()
					: insurancePoliciesHelper.standardPolicyType();
				insurancePoliciesHelper.calculateBonus();
		},
		onInsuredDaysChange : function() {
			var f = $(insurancePoliciesHelper.tags.newPolicyForm);
			var policyType = f.find('select[name="'+insurancePoliciesHelper.fields.names.policyType+'"]').val();
				if(parseInt(policyType) != 2) return false;
				insurancePoliciesHelper.calculateBonus();
		},
		onBirthdayChange : function() {
			var f     = $(insurancePoliciesHelper.tags.newPolicyForm);
			var bDate = f.find('input[name="'+insurancePoliciesHelper.fields.names.birthdayPicker+'"]').val();
				bDate = moment(bDate, 'DD/MM/YYYY');
				insurancePoliciesHelper.config.Prices.DayPrice = (insurancePoliciesHelper.helpers.getAge(bDate) >= insurancePoliciesHelper.config.Ages.DayPriceChangeAgePoint)
												? insurancePoliciesHelper.config.Prices.DayPrices.AfterAgePointPrice
												: insurancePoliciesHelper.config.Prices.DayPrices.BeforeAgePointPrice;
				insurancePoliciesHelper.calculateBonus();
		}
	},
	helpers : {
		getAge : function(d) {
			return moment().diff(moment(d, insurancePoliciesHelper.config.DateFormat), 'years');
		},
		nextYear : function(d) {
			d = new Date(insurancePoliciesHelper.helpers.parseUIDate(d));
			d.setFullYear(d.getFullYear() + 1);
			return d;
		},
		parseUIDate :  function(d) {
			var b = d.split('/'); return b[1] + '-' + b[0] + '-' + b[2];  
		},
		diffYears : function(sDate, eDate) {
			
		},
	},
	saveLegalInfo : function(o) {
		if(!_form.validateFormInputs(o)) {
			return false;
		}

		$(insurancePoliciesHelper.fields.legalInfoForm).addLoader();
		
		$.post(CONFIG.POLICIES.saveLegalInfo, $(o).serialize(), function(data){
			$(insurancePoliciesHelper.fields.legalInfoForm).removeLoader();
		
			if(data.StatusCode == 1){
				jAlert(data.StatusMessage, null, function(){
					location.reload();
				});
				return false;
			} 
			
			jAlert(data.StatusMessage);	
		});

		return false;
	},
	requestActivation : function(){
		$.post(CONFIG.POLICIES.requestActivation, {}, function(data){		
			if(data.StatusCode == 1){
				jAlert(data.StatusMessage, null, function(){
					$(insurancePoliciesHelper.tags.companyActivationBtn).replaceWith(insurancePoliciesHelper.config.Markup.Admin.Blocks.ActivationRequestSent);
					// location.reload();
				});
				return false;
			} 
			
			jAlert(data.StatusMessage);	
		});

		return false;
	},
	initLegalUserForm : function() {
		$(insurancePoliciesHelper.fields.legalCompNameGeo).geokbd();
		$(insurancePoliciesHelper.fields.legalAddressNameGeo).geokbd();
	},
	initNewPolicyForm : function() {
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.insuredNameEng+'"]').uCase();
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.insuredLastnameEng+'"]').uCase();
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.passportNumber+'"]').uCase();
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.address+'"]').uCase();
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.insuredNameGeo+'"]').geoInput();
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.insuredLastnameGeo+'"]').geoInput();
		
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]').datepicker({
			changeMonth : true,
			numberOfMonths : 2,
			dayNames: insurancePoliciesHelper.datepickerLangs.dNames,
			dayNamesShort: insurancePoliciesHelper.datepickerLangs.dNamesShort,
			minDate: 1,
			dayNamesMin: insurancePoliciesHelper.datepickerLangs.dNamesShort,
			monthNames : insurancePoliciesHelper.datepickerLangs.mNames,
			monthNamesShort : insurancePoliciesHelper.datepickerLangs.mNames,
			dateFormat: 'dd/mm/yy',
			dayOfWeekStart: 1,
			onSelect: function(dateText) {
				insurancePoliciesHelper.calculateDays();
				insurancePoliciesHelper.calculateBonus();
			} 
		}).datepicker('setDate', 1);

		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.dateFromPicker+'"]').datepicker({
			changeMonth : true,
			dateFormat: 'dd/mm/yy',
			dayNames: insurancePoliciesHelper.datepickerLangs.dNames,
			dayNamesShort: insurancePoliciesHelper.datepickerLangs.dNamesShort,
			minDate: 0,
			dayNamesMin: insurancePoliciesHelper.datepickerLangs.dNamesShort,
			monthNamesShort : insurancePoliciesHelper.datepickerLangs.mNames,
			monthNames : insurancePoliciesHelper.datepickerLangs.mNames,
			numberOfMonths : 2, 
			dayOfWeekStart: 1,
			onSelect: function(dText) {
				var f = $(insurancePoliciesHelper.tags.newPolicyForm); 
				if(f.find('select[name="'+insurancePoliciesHelper.fields.names.policyType+'"]').val() == 2) {
					f.find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]').datepicker('setDate', insurancePoliciesHelper.helpers.nextYear(dText));
				}
				insurancePoliciesHelper.calculateDays();
				insurancePoliciesHelper.calculateBonus();
			} 
		}).datepicker('setDate', new Date());

		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.birthdayPicker+'"]').datepicker({
			dateFormat: 'dd/mm/yy',
			yearRange: "-100:+0",
			dayNames: insurancePoliciesHelper.datepickerLangs.dNames,
			dayNamesShort: insurancePoliciesHelper.datepickerLangs.dNamesShort,
			dayNamesMin: insurancePoliciesHelper.datepickerLangs.dNamesShort,
			monthNames : insurancePoliciesHelper.datepickerLangs.mNames,
			monthNamesShort : insurancePoliciesHelper.datepickerLangs.mNames,
			changeYear: true, 
			changeMonth : true,
			showWeek : false,
			// beforeShowDay: function(date){ return [date.getDay() == 6 || date.getDay() == 0,""]},
			onSelect: function(dateText) {
				insurancePoliciesHelper.eventListeners.onBirthdayChange();
			},
			onClose: function() { }
		});

		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]').keypress(function(event){ event.preventDefault(); });
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.dateFromPicker+'"]').keypress(function(event){ event.preventDefault(); });
		$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.birthdayPicker+'"]').keypress(function(event){ event.preventDefault(); });
	},
	initFilterForm : function(){
		var f = $(insurancePoliciesHelper.tags.filterForm);
		var sDate = (f.find('input[name="'+insurancePoliciesHelper.fields.names.filterStartDate+'"]').data('current'))  ? f.find('input[name="'+insurancePoliciesHelper.fields.names.filterStartDate+'"]').data('current') : '';
		var eDate = (f.find('input[name="'+insurancePoliciesHelper.fields.names.filterEndDate+'"]').data('current'))  ? f.find('input[name="'+insurancePoliciesHelper.fields.names.filterEndDate+'"]').data('current') : '';
			
			f.find('input[name="'+insurancePoliciesHelper.fields.names.filterStartDate+'"]').datepicker({
				maxDate : new Date(),
				changeMonth : true,
				dayNames: insurancePoliciesHelper.datepickerLangs.dNames,
				dayNamesShort: insurancePoliciesHelper.datepickerLangs.dNamesShort,
				dayNamesMin: insurancePoliciesHelper.datepickerLangs.dNamesShort,
				monthNames : insurancePoliciesHelper.datepickerLangs.mNames,
				monthNamesShort : insurancePoliciesHelper.datepickerLangs.mNames,
				numberOfMonths : 1,
				dayOfWeekStart: 1,
			}).datepicker('setDate', sDate);

			f.find('input[name="'+insurancePoliciesHelper.fields.names.filterEndDate+'"]').datepicker({
				maxDate : new Date(),
				changeMonth : true,
				dayNames: insurancePoliciesHelper.datepickerLangs.dNames,
				dayNamesShort: insurancePoliciesHelper.datepickerLangs.dNamesShort,
				dayNamesMin: insurancePoliciesHelper.datepickerLangs.dNamesShort,
				monthNames : insurancePoliciesHelper.datepickerLangs.mNames,
				monthNamesShort : insurancePoliciesHelper.datepickerLangs.mNames,
				numberOfMonths : 1,
				dayOfWeekStart: 1,
			}).datepicker('setDate', eDate);
	},
	standardPolicyType : function() {
		var f = $(insurancePoliciesHelper.tags.newPolicyForm);
			f.find('input[name="'+insurancePoliciesHelper.fields.names.dateFromPicker+'"]')
				.datepicker('setDate', new Date());
			f.find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]')
				.removeAttr('disabled').datepicker('setDate', 1);
			f.find('select[name="'+insurancePoliciesHelper.fields.names.insuredDays+'"]')
				.attr('disabled', 'disabled').val(2).find('option').show()	
	},
	multiPolicyType : function() {
		var f 	  = $(insurancePoliciesHelper.tags.newPolicyForm); 
		var nYear = new Date();
			nYear.setFullYear(nYear.getFullYear() + 1); 	
			f.find('input[name="'+insurancePoliciesHelper.fields.names.dateFromPicker+'"]').datepicker('setDate', new Date());
			f.find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]')
				.attr('disabled', 'disabled')
				.datepicker('setDate', nYear);
			f.find('select[name="'+insurancePoliciesHelper.fields.names.insuredDays+'"] option').hide()
				.siblings('option[value="30"], option[value="90"], option[value="180"]').show()
				.parents('select').val(180).removeAttr('disabled');

	},
	idCodeRequired : function(required){
		var o = $(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.idCode+'"]');
		if(!required) {
			o.removeAttr('data-error');
			o.removeAttr('data-type-error');
		} else {
			o.attr('data-error', 'მიუთითეთ პირადი ნომერი');
			o.attr('data-type-error', 'გთხოვთ მიუთითოთ ვალიდური პირადი ნომერი');
		}
	},

	calculateBonus : function() {
		var f 	 	= $(insurancePoliciesHelper.tags.newPolicyForm);
		var daysNum = f.find('select[name="'+insurancePoliciesHelper.fields.names.insuredDays+'"]').val();
			if(daysNum < insurancePoliciesHelper.config.Days.MinDaysNum) {
				if(insurancePoliciesHelper.config.Prices.DayPrice == insurancePoliciesHelper.config.Prices.DayPrices.AfterAgePointPrice) {
					var cValue = (insurancePoliciesHelper.config.Prices.MinDayPrices.AfterAgePointPrice).toFixed(2);
				} else {
					var cValue = (insurancePoliciesHelper.config.Prices.MinDayPrices.BeforeAgePointPrice).toFixed(2);
				}
			}  else {
				var policyTypeBonus = f.find('select[name="'+insurancePoliciesHelper.fields.names.policyType+'"] option:selected').data('bonus');
				var cValue = parseFloat(policyTypeBonus * daysNum * insurancePoliciesHelper.config.Prices.DayPrice);
					cValue = cValue.toFixed(2);
			}

			if(f.find('input[name="'+insurancePoliciesHelper.fields.names.luggage+'"]').is(':checked')) { 
				cValue = parseFloat(cValue) + parseFloat(insurancePoliciesHelper.config.Prices.LugaggePrice); 
			}

			if(f.find('input[name="'+insurancePoliciesHelper.fields.names.flightDelay+'"]').is(':checked')) { 
				cValue = parseFloat(cValue) + parseFloat(insurancePoliciesHelper.config.Prices.FlightDelayPrice); 
			}

			cValue = parseFloat(cValue);

			f.find(insurancePoliciesHelper.tags.minBonusValue).html(cValue + ' ლარი/GEL');
			f.find('input[name="'+insurancePoliciesHelper.fields.names.userBonus+'"]').val(cValue);
			insurancePoliciesHelper.config.Prices.CurrentMinPrice = cValue;
	},
	calculateDays : function(){
		var f = $(insurancePoliciesHelper.tags.newPolicyForm);
			if(f.find('select[name="'+insurancePoliciesHelper.fields.names.policyType+'"]').val() == 2)
				return false;
		var sDate    = moment(f.find('input[name="'+insurancePoliciesHelper.fields.names.dateFromPicker+'"]').val(), insurancePoliciesHelper.config.DateFormat);
		var eDate    = moment(f.find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]').val(), insurancePoliciesHelper.config.DateFormat);
		var daysDiff = eDate.diff(sDate, 'days') + 1;	
			if(daysDiff > insurancePoliciesHelper.config.Days.MinThreshold) daysDiff = insurancePoliciesHelper.config.Days.MinThreshold;
			f.find('select[name="'+insurancePoliciesHelper.fields.names.insuredDays+'"]').val(daysDiff);
	},	
	newPolicy : function(o) {
		if(!_form.validateFormInputs(o)) {
			return false;
		}

		this.calculateBonus();

		var f = $(insurancePoliciesHelper.tags.newPolicyForm);

		if(parseFloat(insurancePoliciesHelper.config.Prices.CurrentMinPrice) > 
			parseFloat(f.find('input[name="'+insurancePoliciesHelper.fields.names.userBonus+'"]').val())) {
			jAlert('თქვენს მიერ მითითებული პრემია ნაკლებია მინიმალურ პრემიაზე', null , function(){
				f.find('input[name="'+insurancePoliciesHelper.fields.names.userBonus+'"]').focus();
			});

			return false;
		}

		var sDate = moment(f.find('input[name="'+insurancePoliciesHelper.fields.names.dateFromPicker+'"]').val(), insurancePoliciesHelper.config.DateFormat);
		var eDate = moment(f.find('input[name="'+insurancePoliciesHelper.fields.names.dateToPicker+'"]').val(), insurancePoliciesHelper.config.DateFormat);

			if(eDate.diff(sDate) < 0) {
				jAlert('სადაზღვეო პერიოდის დასასრული შეუძლებელია იყოს დასაზღვეო პერიოდის დასაწყისზე მეტი');
				return false;
			}

		var mobileInput = f.find('input[name="'+insurancePoliciesHelper.fields.names.mobile+'"]');
		var mobileValue = mobileInput.val();
			if((mobileValue != '') && (!$.isNumeric(mobileValue) || mobileValue.substring(0,1) != '5' || mobileValue.length != 9)) {
					mobileInput.parent().append('<p class="input-error">' + mobileInput.data('type-error') + '</p>');
				return false;
			} 

		var idInput = f.find('input[name="'+insurancePoliciesHelper.fields.names.idCode+'"]');
		var idValue = idInput.val();
			if((idValue != '') && (!$.isNumeric(idValue) || idValue.length != 11)) {
					idInput.parent().append('<p class="input-error">' + idInput.data('type-error') + '</p>');
				return false;
			} 

		var birthdayInput = f.find('input[name="'+insurancePoliciesHelper.fields.names.birthdayPicker+'"]');
		var birthdayValue = birthdayInput.val();
			if(birthdayValue == '') {
				jAlert('სადაზღვეო პერიოდის დასასრული შეუძლებელია იყოს დასაზღვეო პერიოდის დასაწყისზე მეტი', null, function(){
					birthdayInput.focus();
				});
				return false;
			}

		// var passportInput = f.find('input[name="'+insurancePoliciesHelper.fields.names.passportNumber+'"]');
		// var passportValue = passportInput.val();
		// 	if(!$.isNumeric(passportValue)) {
		// 		jAlert('პასპორტის ნომერი არ არის ვალიდური.', null, function(){
		// 			birthdayInput.focus();
		// 		});
		// 		return false;
		// 	}

		
			
		f.addLoader();
		
		$.post(CONFIG.POLICIES.generatePolicy, f.serialize(), function(data){
			f.removeLoader();
				f.removeLoader();
				if(data.StatusCode != 1) {
					jAlert(data.StatusMessage);
				} else { 
					jDialog(
						'პოლისი წარმატებით დაგენერირდა',
						{
							print_policy: {
								text: 'პოლისის გადმოწერა',
								class: 'btn btn-green',
								click: function(){
									goToLink(data.Data.PolicyUrl, '_blank');
								}
							},
							close: {
								text: 'დახურვა',
								class: 'btn btn-green',
								click: function(){
									location.reload();
								}
							}
							// ,
							// my_policies: {
							// 	text: 'ჩემი პოლისები',
							// 	class: 'btn btn-green',
							// 	click: function(){
							// 		goToLink(CONFIG.POLICIES.policies);
							// 	}
							// }
						},
						null,
						function(){
							location.reload();
						}
					);
				}
				// $('#NewPolicyForm').find('.new-policy-tbc-info').removeLoader();
		});
		
		return false;
	},
	addOperator :  function(o) {
		if(!_form.validateFormInputs(o)) {
			return false;
		}

		var l = $(insurancePoliciesHelper.tags.tbcPageBlock);
			l.addLoader();	
		$.post(CONFIG.POLICIES.addOperator, $(o).serialize(), function(data){
			l.removeLoader();
			if(data.StatusCode == 1) {
				jAlert(data.StatusMessage, null, function(){
					location.reload();
				});
				return false;
			} else {
				jAlert(data.StatusMessage);
			}
		});

		return false;
	},
	deleteOperator : function(opID) {
		if(!opID || opID == 0) {
			jAlert('მოხდა შეცდომა');
			return false;
		}

		jConfirm('გსურთ მოცემულ ოპერატორის წაშლა?', null, function(e){
			if(e){
				var l = $(insurancePoliciesHelper.tags.tbcPageBlock);
					l.addLoader();	
				$.post(CONFIG.POLICIES.deleteOperator, { OperatorID : opID}, function(data){
					l.removeLoader();
					if(data.StatusCode == 1) {
						jAlert(data.StatusMessage, null, function(){
							$(insurancePoliciesHelper.tags.operatorsList).find('tr[data-op-id="'+opID+'"]').remove();
						});
					} else {
						jAlert(data.StatusMessage);
					}
				});
			}
		});
	},
	//Admin Related
	manageCompanyModal : function(userID) {
		popup(CONFIG.POLICIES.ADMIN.manageCompanyForm, 'პაროლის აღდგენა');
	},
	setCompanyStatus : function(userID, statusID) { 
		var o = $('['+this.tags.tbcCompListItem+'="'+userID+'"]');
			o.parents('table').addLoader();
		$.post(CONFIG.POLICIES.ADMIN.setCompanyStatus, {UserID : userID, StatusID : statusID}, function(data){
			if(data.StatusCode == 1){
					location.reload();
					// o.parents('table').removeLoader();
					// o.find(insurancePoliciesHelper.tags.tbcCompStatusBlock).html(statusID == 1 ? insurancePoliciesHelper.config.Markup.Admin.Blocks.CompanyUserActive : insurancePoliciesHelper.config.Markup.Admin.Blocks.CompanyUserInactive).parents('['+insurancePoliciesHelper.tags.tbcCompListItem+'="'+userID+'"]')
					//  .find(insurancePoliciesHelper.tags.manageCompStatusBtn).replaceWith((statusID == 1 ? insurancePoliciesHelper.config.Markup.Admin.Actions.DeactivateCompanyUser : insurancePoliciesHelper.config.Markup.Admin.Actions.ActivateCompanyUser).replace('%s', userID));				
					// return false;
			} 
			
			jAlert(data.StatusMessage);	
		});
	}
}

$(document).ready(function(){
	insurancePoliciesHelper.initLegalUserForm();
	insurancePoliciesHelper.initNewPolicyForm();
	insurancePoliciesHelper.initFilterForm();

	$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.notResident+'"]').on('change', function(){
		insurancePoliciesHelper.eventListeners.onResidentChange();
	});

	$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.limitCurrency+'"]').on('change', function(){
		insurancePoliciesHelper.eventListeners.onLimitCurrencyChange();
	});

	$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.luggage+'"]').on('change', function(){
		insurancePoliciesHelper.calculateBonus();
	});

	$(insurancePoliciesHelper.tags.newPolicyForm).find('input[name="'+insurancePoliciesHelper.fields.names.flightDelay+'"]').on('change', function(){
		insurancePoliciesHelper.calculateBonus();
	});

	$(insurancePoliciesHelper.tags.newPolicyForm).find('select[name="'+insurancePoliciesHelper.fields.names.policyType+'"]').on('change', function(){
		insurancePoliciesHelper.eventListeners.onPolicyTypeChange();
	});

	$(insurancePoliciesHelper.tags.newPolicyForm).find('select[name="'+insurancePoliciesHelper.fields.names.insuredDays+'"]').on('change', function(){
		insurancePoliciesHelper.eventListeners.onInsuredDaysChange();
	});
});